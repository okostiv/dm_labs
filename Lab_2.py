#!/usr/bin/env python

from math import log
while True:
    funval = input('введiть набiр значень функцiї\n').replace(" ", "")
    if funval == 'quit':
        break
    if funval.replace('0','').replace('1',''):
        print('в наборi присутнi недопустимi символи')
        continue
    power = log(len(funval))/log(2)
    if power.is_integer():
        power = int(power)
    else:
        print('в наборi невiдповiдна кiлькiсть значень')
        continue
    rslt = False
    for i in range(len(funval)):
        if i < len(funval)/2:
            rslt = (funval[i] is not funval[-i-1])   
        i_bin = bin(i)[2:]
        key = '0'*(power-len(i_bin)) + i_bin
        print(key + ": " + funval[i])
    print('функцiя -' + ('' if rslt else ' не') + ' самодвоїста') 
