def print_jarl(jarl):
    print('jarl:{')
    _jkeys = list(jarl.keys())
    _jkeys.sort()
    for _from in _jkeys:
        _jkkeys = list(jarl[_from].keys())
        _jkkeys.sort()
        for _to in _jkkeys:
            print(' (' +_from+ '->' +_to+ "):", jarl[_from][_to]) 
    print('}')

def turn_back(_to, jarl):
    for _from in jarl.keys():
        if _to in jarl[_from].keys():
            if jarl[_from][_to][1] > 0:
                return _from 

def find_path(_from, _to, jarl, path):
    #from pprint import pprint
    #pprint(jarl)
    
    #print(path, end = "") 
    #input()
    if _from == _to:
        return path
    else:
        while jarl[_from]:
            _next = list(jarl[_from].keys()).pop()
            pow = jarl[_from][_next]
            del jarl[_from][_next]
            
            if pow[0] <= pow[1]:
                continue                      
            
            if _next not in path:
                path.append((_from,_next))
                if find_path(_next, _to, jarl, path):
                    return path
                else:
                    path.pop()
        else:
            while turn_back(_from, jarl):
                _prev = turn_back(_from, jarl)
                pow = jarl[_prev][_from]
                del jarl[_prev][_from]

                if not pow[1]: continue

                if _prev not in path:
                    path.append((_from, _prev)) #not in pairs
                    if find_path(_prev, _to, jarl, path):
                        return path
                    else:
                        path.pop()
        return

def saturate_path(path, jarl, pairs):
    def pow_sat(pair): #power_saturation
        x,y = pair
        #pprint(jarl)
        #print([x,y])
        return (jarl[x][y][0] - jarl[x][y][1]) if [x,y] in pairs else jarl[y][x][1]
        
    xmin,ymin = min(path, key = pow_sat)
    min_flow = pow_sat((xmin,ymin))

    for px, py in path:
        if [px,py] in pairs:
            jarl[px][py][1] += min_flow
        else:
            jarl[py][px][1] -= min_flow

    print('mf: ', (xmin, ymin), '= ', min_flow, "\n")
    return min_flow
    #print_jarl(jarl)
    #input()
