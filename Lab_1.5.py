#!/usr/bin/env python
class Matrix:
    def __init__(self, f):
        self.array = []
        for line in f.readlines():
            self.array.append([int(j) for j in line.split()])
        self.dim = len(self.array)
    def __repr__(self):
        rat = "class Matrix object:" + "\n"
        for i in self.array:
            rat+= str(i) + "\n"
        return rat
    def simetricity(self, **qwargs):
        if 'anti' not in qwargs.keys(): 
            qwargs['anti'] = False  
        for i in range(self.dim):
            for j in range(i+1, self.dim):
                if (qwargs['anti']     and (self.array[i][j] == self.array[j][i])) \
                or (not qwargs['anti'] and (self.array[i][j] != self.array[j][i])): 
                    return False
        return True
    def transitivity(self):
        self.transitivity_break = []
        for i in range(0, self.dim): #just
            for j in range(0, self.dim):#for
                for k in range(0, self.dim):#all
                    if i!=j and j!=k and i!=k: # except diagonal
                        if self.array[i][j]: # if ArB
                            if self.array[j][k]: # and BrC
                               if self.array[i][k]: #then ArC
                                   continue
                               else:
                                  self.transitivity_break.append([i,j,k]) # [i][j], [j][k], [i][k]
        if self.transitivity_break:
            return False
        else:
            return True
if __name__ == '__main__':
    Mx = Matrix(open(r'Lab_1.5.matrix', 'r'))
    print('{0}\nsimetricity = {1}\nantisimetricity = {2}\ntransitivity = {3}\n'.format(Mx, Mx.simetricity(), Mx.simetricity(anti=True), Mx.transitivity()))
    if not Mx.transitivity():
        print('transitivity breaks at {}'.format(Mx.transitivity_break));
    input("\nPress Enter to exit.")
