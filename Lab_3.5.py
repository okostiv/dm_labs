def mult(matrix1,matrix2 = None):
    if not matrix2:
        matrix2=matrix1
    if len(matrix1[0]) != len(matrix2):
        print ('Matrices must be m*n and n*p to multiply!')
    else:
        new_matrix = [[0 for i in matrix1]for i in matrix2[0]]
        for i in range(len(matrix1)): 
            for j in range(len(matrix2[0])): 
                for k in range(len(matrix2)): 
                    new_matrix[i][j] += matrix1[i][k]*matrix2[k][j]
        return new_matrix

k_list = {tuple(map(int, pair)) for pair in open('Lab_3.5.k_list').read().split()}
j_size = max(max(k_list, key=max))+1
k_jarl = [[int((row,col) in k_list) for col in range(j_size)] for row in range(j_size)]
k_jarl_2 = mult(k_jarl)
j_rank = [[k+l for (k,l) in zip(i,j)] for (i,j) in zip(k_jarl,k_jarl_2)]
for i in range(j_size): print(i, sum(j_rank[i]), sep=": ")

print()
for mx in (k_jarl, k_jarl_2, j_rank):
    for row in mx: 
        print(row)
    print()
