from itertools import chain
from pprint import pprint
from copy import copy, deepcopy

from Lab_4_21 import data
from Lab_4_func import *

pairs  = [pair  for pair  in map(list, data.pop(0).split())] #[[0,1], [1,2], ..]
powers = [power for power in map(int,  data.pop(0).split())] #[11, 17, ...]
vertices = {vert for vert in chain(*pairs)} 
jarl = {_from:{_to:[_power,0] for (_f,_to),_power in zip(pairs, powers) if _f == _from} for _from in vertices}
max_flow = 0

print_jarl(jarl)
print()

while True:
    path = []
    if find_path('0', 'z', deepcopy(jarl), path):
        print('path:', path)        
        max_flow+=saturate_path(path, jarl, pairs)
    else:
        break
            
print_jarl(jarl)
print('max flow = ', max_flow)
input()