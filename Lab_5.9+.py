literals, operators = ('A', 'B', 'C'), ('*', '/','+', '-')
alphabet = literals + operators + ('(', ')')

def reduce(_s):
    s,a = _s, 'a'
    for l in literals:  s = s.replace(l, a)
    for o in operators: s = s.replace (a+o+a, a)
    s = s.replace('({})'.format(a), a)
    if s != _s:
        reduce(s)
    else:
        if s != a: raise Exception('unable to reduce')
        else: return 
        
def __main__():
    print('Type expression using alphabet: [{}]'.format(', '.join(alphabet)))
    while True:
        try:
            expression = input('type your expression: ').replace(' ', '')
            if not expression or expression == 'quit':
                break
            for char in expression:
                if char not in alphabet:
                    raise Exception('has odd signs: ' +char+ '.')
            reduce(expression)
            print('Expresion is correct.')
        except Exception as ex:
            print('Expression is wrong: ' + str(ex))

if __name__ == '__main__':
    __main__()
